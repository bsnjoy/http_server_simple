'use strict';

let fs = require('fs'),
	http = require('http'),

	LISTEN_HOSTNAME = '0.0.0.0',
	LISTEN_PORT = 80,

	DUMP_FILE_PATH = 'dumpdata.txt',

	ACK_HTTP_CODE = 200,
	ACK_CONTENT_TYPE = 'text/html; charset=utf-8';
var ACK_MESSAGE = 'inserted events: 2254785<br>';
var event_id = 1000;


{
	let server = http.createServer((request,response) => {

		// start of new HTTP request
		let requestDataSlabList = [],
			httpMethod = request.method.toUpperCase(),
			requestURI = request.url;

		// summary request details
		console.log(request.rawHeaders);
		console.log(`Incoming request\nMethod: ${httpMethod}\nURI: ${requestURI}`);

		// wire up request events
		request.on('data',(data) => {

			// add received data to buffer
			requestDataSlabList.push(data);
		});

		request.on('end',(data) => {

			// send response to client

			ACK_MESSAGE = 'inserted events: ' + event_id++ + '<br>';
			response.writeHead(
				ACK_HTTP_CODE,
				{	'Content-Length': ACK_MESSAGE.length.toString(),
					'Content-Type': ACK_CONTENT_TYPE
				}
			);

			response.end(`${ACK_MESSAGE}`);

			// write/append received request to file
			let headerItemList = [],
				dataSlab = requestDataSlabList.join('');

			for (let headerItem of Object.keys(request.headers).sort()) {
				headerItemList.push(`\t${headerItem}: ${request.headers[headerItem]}`);
			}

			fs.appendFile(
				DUMP_FILE_PATH,
				`Method: ${httpMethod}\nURI: ${requestURI}\n` +
				`Headers:\n${headerItemList.join('\n')}\n\n${dataSlab}\n\n\n`,
				(err) => {

					// end of HTTP request
					console.log(`End of request, ${dataSlab.length} bytes received.\n`);
				}
			);
		});
	});

	// start listening server
	console.log(`Listening on ${LISTEN_HOSTNAME}:${LISTEN_PORT}\n`);
	server.listen(LISTEN_PORT,LISTEN_HOSTNAME);
}
